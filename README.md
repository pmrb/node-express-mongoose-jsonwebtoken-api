# Node.js small api

Small scalable api for small projects or personal use.

It uses babel-node to transpile the es6 features to plain js.

It contains Express, Mongoose, JsonWebToken

## Requirements

* Babel https://babeljs.io/

### How to use

* Git clone https://github.com/pedromrb/node-express-mongoose-jsonwebtoken-api.git dirname
* ``` $ cd dirname ```
* ``` $ npm i ```
* ``` $ npm start ```
* open your browser on http://localhost:8080
