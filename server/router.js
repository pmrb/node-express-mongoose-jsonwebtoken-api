import express from 'express'
import Pages from './routes/pages'
import User from './routes/user'
import Auth from './routes/auth'

export default () => {
  let router = express.Router()

  Pages(router)
  Auth(router)
  User(router)

  return router
}
