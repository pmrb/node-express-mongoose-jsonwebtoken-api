import User from '../models/user'
import auth from '../middleware/isAuthenticated'

export default (router) => {
  router.get('/users', auth, (req, res) => {
    User.find({}, (err, users) => {
      res.json(users);
    })
  })

  router.get('/users/seed', (req, res) => {
    let pedro = new User({
      name: 'Pedro Bras',
      password: 'password',
      admin: true
    })

    pedro.save((err) => {
      if (err) throw err

      console.log('User saved successfully')
      res.json({ success: true })
    })
  })
}
