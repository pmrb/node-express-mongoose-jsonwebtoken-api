import User from '../models/user'
import jwt from 'jsonwebtoken'
import config from '../config'

export default (router) => {
  router.post('/authenticate', (req, res) => {
    console.log(JSON.parse(req.body))
    User.findOne({
      name: req.body.name
    }, (err, user) => {
      if (err) throw err;

      if (!user) {
        res.json({
          success: false,
          message: 'Authentication failed. User not found.'
        })
      } else if (user) {
        if (user.password !== req.body.password) {
          res.json({
            success: false, message:
            'Authentication failed. Wrong password.'
          })
        } else {
          let token = jwt.sign(user, config.secret, {
            expiresInMinutes: 1440
          })

          res.json({
            success: true,
            message: 'Authentication successful. You can now use the endpoints',
            token: token
          })
        }
      }
    })
  })
}
